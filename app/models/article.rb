class Article < ApplicationRecord
    has_many :has_categories
    has_many :categories, through: :has_categories
    attr_accessor :category_elements

    # Crear arrglo de ids para guardar las categorias
    def save_categoris
        # category_elements ([1,2,3])
        return has_categories.destroy_all if category_elements.nil? || category_elements.empty?

        has_categories.where.not(category_id: category_elements).destroy_all

        categories_array.each do |category_id|
            # por cada elemento crea un registro en la tabla asociativa (HasCategory)
            # self es como this
            # validar si existe el registro. find_or_create_by si existe lo retorna y si no lo crea
            HasCategory.find_or_create_by(article: self, category_id: category_id)
        end
    end
end
