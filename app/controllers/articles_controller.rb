class ArticlesController < ApplicationController
    before_action :find_article, only: [:show, :edit, :update, :destroy]

    def index
        @article = Article.all
        @categories = Category.all
    end

    def new
        @article = Article.new
    end

    def create
        @article = Article.new(article_param)
        @article.save_categoris
        @article.save

        render json: @article
    end

    def show
    end

    def edit
    end

    def update
        @article.update(article_param)
        @article.save_categoris
        redirect_to :action => :show, :id => @article.id
    end

    def destroy
        @article.destroy

        redirect_to root_path
    end

    private def article_param
        params.permit(:title, :content)
    end

    def find_article
        @article = Article.find(params[:id])
    end
end
