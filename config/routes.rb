Rails.application.routes.draw do
  resources :categories
  # Define your application routes per the DSL in https://guides.rubyonrails.org/routing.html

  # Defines the root path route ("/")
  root to: 'home#index'

  # root "articles#index"

  get 'home' => 'home#index'

  get 'articles' => 'articles#index'

  get 'articles/new' => 'articles#new'
  post 'articles' => 'articles#create'

  get 'articles/:id' => 'articles#show'

  get 'articles/:id/edit' => 'articles#edit'
  put 'articles/:id' => 'articles#update'

  delete 'articles/:id' => 'articles#destroy'
end
